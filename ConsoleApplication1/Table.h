#pragma once
#include "Normal.h"
#include "shablon.h"
#include <stdio.h>
#include <windows.h>
#include <locale>
#include <iostream>
#include <string.h>
#include <list>
#include "Monetary.h"


struct	vklad
{
	int		num;
	Normal	*handle;
};
/*!����� ������� xD */
/// ����� ������� 
class bank					//����� �����
{
private:
	double	_course;
	list	<vklad*> table;				//������� �������
	list	<vklad*>::iterator it;		//�������� ��� ������ �� �������
public:
	///�� �!? ���������� ������� ���� 
	bank(double course = 0) { _course = course; }
	///��������� 
	~bank()
	{
		for (it = table.begin(); table.size(); it = table.begin())
		{
			delete (*it)->handle;
			table.erase(it);
		}
	}
	///����� ���������� ������ ������� ����� � ID ���������� true
	bool Add(Normal * vkl, int n)	//���������� 
	{
		int		i;

		//����, ���� �������, ����� ���� ������������� (���������� ��������)
		//�� ���� �������
		for (it = table.begin(); it != table.end(); it++)
		{
			//������� ������ ������
			i = (*it)->num - n;
			if (i == 0)		//����� - ����� ��� ����. ������������, ��� ��� ������
				return FALSE;
			else if (i > 0)	//������ - ���� ���������
				break;
		}					//��� � ����� ������

		vklad * v = new vklad;
		v->num = n;
		v->handle = vkl;
		table.insert(it, v);	//��������� � �������
		return TRUE;			//����������
	}
	///����� �������� ������ �� ID ���������� false
	bool Del(int n)				//��������
	{
		int		i;

		//���� �� ���� �������
		for (it = table.begin(); it != table.end(); it++)
		{
			//������� ������ ������
			i = (*it)->num - n;
			if (i == 0)
			{
				delete (*it)->handle;
				table.erase(it);
				return true;
			}
			else if (i > 0)	//������ - ���� ���������
				break;
		}
		return false;
	}
	void SetCourse(double course)
	{
		_course = course;
		for (it = table.begin(); it != table.end(); it++)
		{
			if ((*it)->handle->GetType() == monetary)
				((Monetary*)((*it)->handle))->Monetary::SetCourse(course);
		}
	}

	inline double GetCourse() { return _course; }

	vklad * GetVklad(int n)
	{
		int		i;

		for (it = table.begin(); it != table.end(); it++)
		{
			i = (*it)->num - n;
			if (i == 0)
				return (*it);	//����� - ��� �����
			else if (i > 0)
				break;
		}
		return NULL;			//0 - �� �����
	}
	///����� ���� ���� �� ���� �������

	void View()
	{
		cout << endl << "������� �������" << endl;
		cout << "�����: " << table.size() << endl;
		//����� ���� � ������ ������
		for (it = table.begin(); it != table.end(); it++)
		{
			cout << endl << "����� �����:    " << (*it)->num << endl;
			(*it)->handle->View();
		}
	}
	///������ ����� ,������
	vklad * first()
	{
		it = table.begin();
		if (it != table.end())
			return (*it);
		else
			return NULL;
	}
	///��������� ������ ����� ������ ������
	vklad * next()
	{
		it++;
		if (it != table.end())
			return (*it);
		else
			return NULL;
	}
}; 