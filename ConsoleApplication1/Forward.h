#pragma once
#include "Normal.h"
#include <stdio.h>
#include <windows.h>
#include <locale>
#include <iostream>
#include <string.h>
#include <list>
/*! ������� ����� */
/// ������� ����� ������������ ����������
class Forward : public Normal
{
private:
	int		_MonthCount;
	int		_MonthBalance;
public:
	///����������� ������� �����, ������� � �����.
	Forward(double sum, double percent, int months)
		:Normal(sum, percent, forward)
	{
		_MonthCount = months;
		_MonthBalance = months;
	}
	virtual void CalcVklad(VDATE *pdate)
	{
		try
		{
			VDATE	*last = GetLastDate();
			int		days = pdate->day - last->day;
			int		months = pdate->month - last->month;
			int		years = pdate->year - last->year;

			if (!IsOpen())
				throw "��� ��������� ����� ���������� �������� ���������� ����������";

			if (days < 0)
			{
				days += 30;
				months--;
			}
			if (months < 0)
			{
				months += 12;
				years--;
			}
			if (years < 0)
				throw "������� ������ ����";

			int		monthcount = (years * 12 + months) + (days / 30);
			if (monthcount)
			{
				if (monthcount > _MonthBalance)
					monthcount = _MonthBalance;
				int sum = GetSum();
				sum += sum * monthcount * GetPercent() / 100;
				sum = ((double)((int)(sum * 100))) / 100;
				SetSum(sum);

				_MonthBalance -= monthcount;
				last->month += monthcount;
				while (last->month > 12)
				{
					last->month -= 12;
					last->year++;
				}
				SetLastDate(last);
				SetClose(this);
			}
		}
		catch (char * str)
		{
			cout << str << endl;
		}
	}
	inline int	GetTail() { return _MonthBalance; }
	virtual void Close(VDATE *pdate)
	{
		if (IsOpen())
			CalcVklad(pdate);
	}
	virtual void View()
	{
		Normal::View();
		cout << "���� ��������:\t" << _MonthCount << " ���" << endl;
	};
	
};

bool Normal::SetClose(Normal * v)
{
	if (v->GetType() == forward && ((Forward*)v)->GetTail() == 0)
	{
		_open = false;
		return true;
	}
	return false;
}

