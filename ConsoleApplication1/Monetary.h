#pragma once
#include "Normal.h"
/*!����� ��������
*/
/// ����� �������� ������������ ����������
class Monetary : public Normal
{
private:
	char	*_Currency;
	double	_Course;
public:
	///����������� �������� �����,�������,���� � ������.
	Monetary(double sum, double percent, double course, char *currency = "USD")
		:Normal(sum, percent, monetary)
	{
		if (currency)
		{
			_Currency = new char[strlen(currency) + 1];
			strcpy_s(_Currency, strlen(currency) + 1, currency);
		}
		_Course = course;
	}
	///��������
	~Monetary() { delete _Currency; }
	virtual void View()
	{
		char	str[32];

		Normal::View();
		cout << "������:\t\t" << _Currency << endl;
		sprintf_s(str, "%5.2f", _Course);
		cout << "����:\t\t" << str << endl;
		cout << "����� � ������:\t" << GetRub() << endl;
	};
	///����� �������� � �����
	inline double GetRub() { return GetSum() * _Course; }
	///����� ����� ����
	inline void SetCourse(double c) { _Course = c; }
	inline char * GetCurrency() { return _Currency; }
};