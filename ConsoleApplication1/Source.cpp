#include "Table.h"
#include "Forward.h"
#include <windows.h>
#include <locale>
#include <iostream>
#include <string.h>
#include <list>
#include <stdio.h>

void CreateVklad();
void GetVklad();
void CloseVklad();
void ModifyVklad();
void GetInfo();
void CalcUSD();
void Exit();

void PutMoney(vklad	*);
void GetMoney(vklad	*);
void GetPercent(vklad *);
void ChangeCourse(vklad *);
void Exit(vklad *);
int GetNum();
int ViewMenu(char**, int);

bank *b = NULL;		//��� �����

char * mesMain[] =		//��������� ����
{
	"������� ����� ����",
	"�������� ��������� �����",
	"������� ����",
	"�������� ����",
	"��� �����",
	"�������� ���� ������� � ������",
	"�����"
};
//���������� ������� ����
int MesMainCount = sizeof(mesMain) / sizeof(mesMain[0]);
//������� ��������� ������� ����
void(*funcMain[])() = { CreateVklad, GetVklad, CloseVklad,
ModifyVklad, GetInfo, CalcUSD, Exit };

char * mesModify[] =
{
	"������ ������",
	"����� ������",
	"����� ��������",
	"�������� ����",
	"�����"
};
//���������� ������� ����
int MesModifyCount = sizeof(mesModify) / sizeof(mesModify[0]);
//������� ��������� ������� ����
void(*funcModify[])(vklad*) = { PutMoney, GetMoney, GetPercent, ChangeCourse, Exit };

void CreateVklad()	//�������� ������
{
	char	str[64] = { 0 };
	int		num = 0;
	int		months = 0;
	double	sum = 0;
	double	percent = 0;
	double	course = 0;

	if (b == NULL)		//�������� ������� �����
	{					//��� - ��������
		cout << endl << "������� ����. ������� ���� USD: ";
		do
		{
			cin.getline(str, sizeof(str), '\n');
			course = atof(str);
		} while (str[0] == 0 || course == 0);

		b = new bank(course);				//������� ����
	}
	else
		course = b->GetCourse();			//���� ����� � �����

											//�������� ��� �����
	cout << endl << "������� ��� �����:" << endl;
	v_type type = (v_type)(ViewMenu(typeStr, type_count) - 1);

	//����� �����
	cout << "������� ����� �����: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		num = atol(str);
	} while (str[0] == 0 || num == 0);

	//�����
	cout << "������� �����: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		sum = atof(str);
	} while (str[0] == 0 || sum == 0);

	//�������
	cout << "������� �������: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		percent = atof(str);
	} while (str[0] == 0 || percent == 0);

	switch (type)
	{
	case normal:
		b->Add(new Normal(sum, percent), num);
		break;
	case monetary:
		b->Add(new Monetary(sum, percent, course), num);
		break;
	case forward:
		//������� ����� �������
		cout << "������� ���� �������� (� �������): ";
		do
		{
			str[0] = 0;
			cin.getline(str, sizeof(str), '\n');
			months = atol(str);
		} while (str[0] == 0 || months == 0);
		b->Add(new Forward(sum, percent, months), num);
	}
}

void GetVklad()
{
	char	str[64] = { 0 };
	int		num = 0;
	vklad	*vkl;
	//����� �����
	cout << "������� ����� �����: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		num = atol(str);
	} while (str[0] == 0 || num == 0);
	vkl = b->GetVklad(num);
	if (vkl)
	{
		vkl->handle->CalcVklad(vkl->handle->GetCurrentDate());
		vkl->handle->View();
	}
	else
		cout << "������ ����� ���" << endl;
}

void CloseVklad()
{
	char	str[64] = { 0 };
	int		num = 0;
	vklad	*vkl;

	cout << "������� ����� �����: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		num = atol(str);
	} while (str[0] == 0 || num == 0);
	vkl = b->GetVklad(num);
	if (vkl)
	{
		vkl->handle->Close(vkl->handle->GetCurrentDate());
	}
	else
		cout << "������ ����� ���" << endl;
}

void ModifyVklad()
{
	char	str[64] = { 0 };
	int		ret, num = 0;
	vklad	*vkl;

	cout << endl << "������� ����� �����: ";
	do
	{
		str[0] = 0;
		cin.getline(str, sizeof(str), '\n');
		num = atol(str);
	} while (str[0] == 0 || num == 0);
	vkl = b->GetVklad(num);
	if (vkl)
	{
		if (vkl->handle->IsOpen())
		{
			do
			{
				ret = ViewMenu(mesModify, MesModifyCount);
				funcModify[ret - 1](vkl);
				if (ret == MesModifyCount)
					break;
				cout << endl;
			} while (ret);
		}
		else
			cout << "���� ������" << endl;
	}
	else
		cout << "������ ����� ���" << endl;
}

void PutMoney(vklad	*vkl)
{
	try
	{
		char	str[64] = { 0 };
		double	sum = 0;

		if (vkl->handle->GetType() == forward)
			throw "�� ������� ����� ������ ������� ������";
		if (!vkl->handle->IsOpen())
			throw "�� �������� ���� ������ ������ ������";
		vkl->handle->CalcVklad(vkl->handle->GetCurrentDate());
		cout << endl << "������� �����: ";
		do
		{
			str[0] = 0;
			cin.getline(str, sizeof(str), '\n');
			sum = atof(str);
		} while (str[0] == 0 || sum == 0);
		vkl->handle->SetSum(vkl->handle->GetSum() + sum);
	}
	catch (char * str)
	{
		cout << str << endl;
	}
}

void GetMoney(vklad	*vkl)
{
	try
	{
		char	str[64] = { 0 };
		double	sum = 0;
		double	sum_vklad;

		vkl->handle->CalcVklad(vkl->handle->GetCurrentDate());
		sum_vklad = vkl->handle->GetSum();

		if ((vkl->handle->GetType() == forward) && vkl->handle->IsOpen())
			throw "C ��������� �������� ������ ������ ����� ������";

		cout << "������� �����, ������� ������ �����: ";
		do
		{
			str[0] = 0;
			cin.getline(str, sizeof(str), '\n');
			sum = atof(str);
		} while (str[0] == 0 || sum == 0);

		if ((vkl->handle->GetType() == forward) && (sum_vklad != sum))
			throw "C� �������� ������ ����� ����� ������ ��� ������ �����";
		if (sum > sum_vklad)
			throw "����� �� ����� ������, ��� �� ������ �����";
		sum_vklad -= sum;
		vkl->handle->SetSum(sum_vklad);
		cout << "�������� " << sum;
		if (vkl->handle->GetType() == monetary)
			cout << " " << ((Monetary*)vkl->handle)->GetCurrency();
		else
			cout << " RUR";
		cout << endl;
	}
	catch (char * str)
	{
		cout << str << endl;
	}
}

void GetPercent(vklad *vkl)
{
	try
	{
		char	str[64] = { 0 };
		double	sum;
		double	sum_vklad;

		if (!vkl->handle->IsOpen())
			throw "C ��������� ������ ������ ����� ��������";

		vkl->handle->CalcVklad(vkl->handle->GetCurrentDate());

		sum_vklad = vkl->handle->GetSum();

		sum = vkl->handle->GetSumPercent();

		vkl->handle->SetSum(sum_vklad - sum);
		cout << "�������� " << sum;
		if (vkl->handle->GetType() == monetary)
			cout << " " << ((Monetary*)vkl->handle)->GetCurrency();
		else
			cout << " RUR";
		cout << endl;
	}
	catch (char * str)
	{
		cout << str << endl;
	}
}

void ChangeCourse(vklad *vkl)
{
	double	course = 0;
	char	str[64] = { 0 };

	cout << "������� ���� USD: ";
	do
	{
		cin.getline(str, sizeof(str), '\n');
		course = atof(str);
	} while (str[0] == 0 || course == 0);

	b->SetCourse(course);
}

void Exit(vklad *vkl) {}

//����� ���������� � ������
void GetInfo()
{
	try
	{
		if (b)
			b->View();
		else
			throw "������� �������� ����!";
	}
	catch (char * str)
	{
		cout << str << endl;
	}
}

void CalcUSD()
{
	vklad * vkl;

	cout << "�������� �������, ���������� �� ���� ������, � USD:" << endl;
	for (vkl = b->first(); vkl != NULL; vkl = b->next())
	{
		vkl->handle->CalcVklad(vkl->handle->GetLastDate());
		if (vkl->handle->GetType() == monetary)
			cout << "���� �" << vkl->num << ":\t" <<
			vkl->handle->GetSum() << " " << ((Monetary*)vkl->handle)->GetCurrency() << endl;
		else
			cout << "���� �" << vkl->num << ":\t" <<
			(double)((int)((vkl->handle->GetSum() / b->GetCourse()) * 100) / 100) << " USD" << endl;
	}
}

void Exit() {}

int GetNum()
{
	int		a;
	cin >> a;
	while (!(cin.good() || cin.eof()) || (a < 0))
	{
		cout << "������� �����! " << endl;
		cin.clear();
		cin.ignore();
		cin >> a;
	}
	return a;
}

int ViewMenu(char** mes, int max)
{
	int ret;
	do
	{
		for (int i = 0; i < max; i++)
			cout << i + 1 << ". " << mes[i] << endl;
		cout << "��� �����: ";
		ret = GetNum();
	} while (ret < 1 || ret > max);
	return ret;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int		ret;
	do
	{
		ret = ViewMenu(mesMain, MesMainCount);
		funcMain[ret - 1]();
		cout << endl;
		if (ret == MesMainCount)
			break;
	} while (ret);

	delete	b;
	system("pause");
	return 0;
}

